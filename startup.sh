#!/bin/bash

rm -rf ~/.vnc/*.pid ~/.vnc/*.log /tmp/.X1* /run/dbus/pid
mkdir -p /root/.vnc/
touch /root/.Xauthority
vncpasswd -f <<< $VNC_PASSWORD > /root/.vnc/passwd
vncserver -PasswordFile /root/.vnc/passwd
dbus-daemon --config-file=/usr/share/dbus-1/system.conf
unlink /usr/share/backgrounds/kali-16x9/default
ln -s /root/Pictures/leto.jpg /usr/share/backgrounds/kali-16x9/default
/usr/share/novnc/utils/novnc_proxy --vnc 127.0.0.1:5901